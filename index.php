<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width", initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">


    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
</head>
<body>
<div class="container-full">
    <div class="row col-md-12">
        <div class="col-md-2 logo">
            <img src="img/mariapolis.jpg" height="70px" width="70px">
        </div>
        <div class="col-md-10 top_title">
            <h1>Mariapolis Piero</h1>
        </div>
    </div>

    <div class="col-md-12 nav_top">
        <ul>
            <li><a href="#">Home</a></li>

            <li><a href="#">About Us</a></li>

            <li><a href="#">Culture and Education</a></li>

            <li><a href="#">Economy</a></li>

            <li><a href="#">Multimedia</a></li>

            <li><a href="#">Facilities</a></li>

            <li><a href="#">Contact Us</a></li>
        </ul>
    </div>

    <div class="col-md-12 nav_second">
        <ul>
            <li><a href="#">Families</a></li>
            <li>|</li>
            <li><a href="#">Youth</a></li>
            <li>|</li>
            <li><a href="#">Priests and Religious</a></li>
            <li>|</li>
            <li><a href="#">Volunteers</a></li>
        </ul>
    </div>

    <div class="col-md-12">
        <div class="carousel_top col-md-10">
            <div><img src="img/mariapolis.jpg"></div>
            <div><img src="img/mariapolis_1.jpg"></div>
        </div>
    </div>

    <div class="col-md-10 bible_verse">

        When he had finished praying, Jesus left with his disciples and crossed the Kidron Valley. On the other side there was a garden, and he and his disciples went into it.

        2 Now Judas, who betrayed him, knew the place, because Jesus had often met there with his disciples. 3 So Judas came to the garden, guiding a detachment of soldiers and some officials from the chief priests and the Pharisees. They were carrying torches, lanterns and weapons.

    </div>

    <div class="row col-md-12">
        <div class="col-md-5 word_of_life">

              <div class="col-md-4 word_of_life_img">
                  <img src="img/mariapolis.jpg" height="80" width="80">
              </div>

              <div class="col-md-8 word_of_life_text">
                  <p>Word of Life</p>
                  <p>I have become all things to all people (1 Cor 9:22)</p>
              </div>

            <div class="upcoming_events">
                <h4>Upcoming Events</h4>

                <hr>
                <div class="event">
                    MAY 25 Emaus Visits Kenya
                </div>

                <div class="event">
                    JAN 29 A visit to this city
                </div>

                <hr>

                <a href="#" class="view_full_calendar"> View full calendar</a>
            </div>

        </div>

        <div class="col-md-7">

            <div>
                <h4>International News</h4>
                <hr>

                <div class="col-md-2 international_news_img">
                    <img src="img/mariapolis.jpg" height="80" width="80">
                </div>

                <div class="col-md-10 international_news_text">
                    <p>Emaus Visits the UN Headquarters</p>
                    <p>Coventry is located in the county of West Midlands but is historically part of Warwickshire. Coventry is the 10th largest city in England and the 13th largest UK city overall</p>

                    <di>
                        <a href="#">Read the full story</a>
                    </di>
                </div>

            </div>



        </div>
    </div>


</div>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="slick/slick.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.carousel_top').slick({
            autoplay: true,
            arrows: true,
            infinite: true,
            speed: 700,
            slidesToShow: 1,
            slidesToScroll: 1
        });
    });
</script>
</body>

</html>